 this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(event => {
        console.log("event", event);
        //Query params ?name=ali gibi ayni sekilde segmentlerde erisilebilir
        this.route
          .queryParams
          .subscribe(params => {
            console.log("Params : ", params)
          });

        //Router Params icin ise, kac tane child oldugunu ya dinamik olarak belirlemen yada sayisini
      // biliyorsan ona gore dallandirman gerekir.
        // http://localhost:4200/admin/dashboard/urunler/edit/59123c96d5607e18f47de0b8?stock=true#showTollbar
        // bu rota icin ornegin , urun id sine ulasmak icin asagidaki gibi cagri yapabilirim
        console.log('router', this.router.routerState.root.firstChild.firstChild.firstChild.snapshot.params);
        //cikti suna benzer olacaktir.

//demo1.component.ts:44 Params :  Object {stock: "true"}
//demo1.component.ts:46 router Object {id: "59123c96d5607e18f47de0b8"}

      })